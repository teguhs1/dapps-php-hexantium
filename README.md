# Function Help
### Javascript :
```javascript
var x = await HxtStd("Help")
```
### Python :
```python
x = HxtStd("Help")
```
### PHP :
```php
$x = HxtStd("Help", array())
```
# Function GenerateKey
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak","GenerateKey")
```
### Python :
```python
x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak","GenerateKey")
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak","GenerateKey")
```
# Function GenerateKeyFromMnemonic
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GenerateKeyFromMnemonic", {
            mnemonic: string
        })
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GenerateKeyFromMnemonic", {
    "mnemonic" => string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GenerateKeyFromMnemonic", array(
    "mnemonic" => string
))
```
# Function BasicRegister
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "BasicRegister", {
    public_key: string,
    signature: [
        {
            d: string
        },
        {
            r: string
        },
        {
            s: string
        }
    ]
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "BasicRegister", {
"public_key": string,
"signature": [
        {
            "d": string
        },
        {
            "r": string
        },
        {
            "s": string
        }
    ]
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "BasicRegister", array(
    "public_key" => string,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    )
))
```
# Function Register
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Register", {
    public_key: string,
    signature: [
        {
            d: string
        },
        {
            r: string
        },
        {
            s: string
        }
    ]
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Register", {
"public_key": "...your public key...",
"signature": [
        {
            "d": string
        },
        {
            "r": string
        },
        {
            "s": string
        }
    ]
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Register", array(
    "public_key" => string,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    )
))
```
# Function Sign 
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Sign", {
    private_key: string,
    data: string,
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Sign", {
    "private_key": string,
    "data": string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Sign", array(
    "private_key" => string,
    "data" => string
))
```
# Function Verify 
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Verify", {
    public_key: string,
    signature: [
        {
            d: string
        },
        {
            r: string
        },
        {
            s: string
        }
    ]
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Verify", {
"public_key": string,
"signature": [
        {
            "d": string
        },
        {
            "r": string
        },
        {
            "s": string
        }
    ]
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "Verify", array(
    "public_key" => string,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    )
))
```
# Function SetProfile 
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "SetProfile", {
    public_key: string,
    signature: [
        {
            d: string
        },
        {
            r: string
        },
        {
            s: string
        }
    ],
    field : string,
    value : string
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "SetProfile", {
"public_key": string,
"signature": [
        {
            "d": string
        },
        {
            "r": string
        },
        {
            "s": string
        }
    ],
"field" => string,
"value" => string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "SetProfile", array(
    "public_key" => string,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    ),
    "field" => string
    "value" => string
))
```
# Function ProposeVerificatorToIssuer
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeVerificatorToIssuer", {
    "verificator": {
        "address": verificator["address"],
        "signature": verificator_s
    },
    "issuer": {
        "address": issuer["address"],
    },
    "owner": {
        "address": owner["address"],
    },
    "certificate": {
        "public" : {
            "title": "TOEFL Prediction Test Report",
        },
        "private" : {
            "listening": "70",
            "structure": "70",
            "reading": "70",
            "total": "210",
        }
    }
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeVerificatorToIssuer", {
"verificator": {
        "address": verificator["address"],
        "signature": verificator_s
    },
    "issuer": {
        "address": issuer["address"],
    },
    "owner": {
        "address": owner["address"],
    },
    "certificate": {
        "public" : {
            "title": "TOEFL Prediction Test Report",
        },
        "private" : {
            "listening": "70",
            "structure": "70",
            "reading": "70",
            "total": "210",
        }
    }
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeVerificatorToIssuer", array(
    "verificator" => array(
        "address" => string,
        "signature" => array(
            "d" => string,
            "r" => string,
            "s" => string
        )
    ),
    "issuer" => array(
        "address" => string
    ),
    "owner" => array(
        "address" => string
    ),
    "certificate" => array()
));
```
# Function ProposeIssuerToOwner
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeIssuerToOwner", {
    "verificator": {
            "address": verificator["address"],
            "signature": verificator_s
        },
        "issuer": {
            "address": issuer["address"],
        },
        "owner": {
            "address": owner["address"],
        },
        "certificate": {
            "public" : {
                "title": "TOEFL Prediction Test Report",
            },
            "private" : {
                "listening": "70",
                "structure": "70",
                "reading": "70",
                "total": "210",
            }
        }
    // issuer: [
    //     {
    //         address: string
    //     },
    //     {
    //         signature: [
    //             {
    //                 d: string
    //             },
    //             {
    //                 r: string
    //             },
    //             {
    //                 s: string
    //             }
    //         ]
    //     }
    // ],
    // ids : []
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeIssuerToOwner", {
    "issuer": [
        {
            "address": string
        },
        {
            "signature": [
                {
                    "d": string
                },
                {
                    "r": string
                },
                {
                    "s": string
                }
            ]
        }
    ],
    "ids" : []
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "ProposeIssuerToOwner", array(
    "issuer" => array(
        "address" => string,
        "signature" => array(
            "d" => string,
            "r" => string,
            "s" => string
        )
    ),
    "ids" => []
));
```
# Function OwnerAccepting
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "OwnerAccepting", {
    owner: [
        {
            address: string
        },
        {
            signature: [
                {
                    d: string
                },
                {
                    r: string
                },
                {
                    s: string
                }
            ]
        }
    ],
    ids : []
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "OwnerAccepting", {
    "owner": [
        {
            "address": string
        },
        {
            "signature": [
                {
                    "d": string
                },
                {
                    "r": string
                },
                {
                    "s": string
                }
            ]
        }
    ],
    "ids" : []
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "OwnerAccepting", array(
    "owner" => array(
        "address" => string,
        "signature" => array(
            "d" => string,
            "r" => string,
            "s" => string
        )
    ),
    "ids" => []
));
```
# Function GetAllBlocks
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetAllBlocks")
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetAllBlocks")
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetAllBlocks");
```
# Function GetBlocks
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlocks", {
    from : int64,
    limit : int16
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlocks", {
    "from" : int64,
    "limit": int16
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlocks", array(
    "from" => int64,
    "limit" => int16
));
```
# Function GetLastBlock
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetLastBlock")
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetLastBlock")
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetLastBlock")
```
# Function GetBlock
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlock", {
    hash : string
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlock", {
    "hash" : string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetBlock", array(
    "hash" => string
));
```
# Function GetDigitalID
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetDigitalID", {
    public_key : string,
    signature : [
        {
            d : string
        },
        {
            r : string
        },
        {
            s : string
        }
    ]
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetDigitalID", {
    "public_key" : string,
    "signature": [
        {
            "d": string
        },
        {
            "r": string
        },
        {
            "s": string
        }
    ]
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetDigitalID", array(
    "public_key" => string,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    )
));
```
# Function GetWorldState
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetWorldState", {
    hash: string
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetWorldState", {
    hash: string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetWorldState", array(
    "hash" => string
));
```
# Function GetCertificate
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificate", {
    viewer: [
        {
            address: string 
        },
        {
        signature : [
            {
                d : string
            },
            {
                r : string
            },
            {
                    s : string
            }
        ]
        }
    ],
    hash: string 
})
```
### Python :
```python
x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificate", {
    viewer: [
        {
            address: string 
        },
            {
                signature : [
                {
                    d : string
                },
                {
                    r : string
                },
                {
                    s : string
                }
            ]
        }
    ],
hash: string 
})
```
### PHP :
```php
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificate", array(
"viewer" => array(
    "address" => string ,
    "signature" => array(
        "d" => string,
        "r" => string,
        "s" => string
    )
), 
"hash" => string
))
```
# Function GetCertificateForPublic
### Javascript :
```javascript
var x = await Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificateForPublic", {
hash: string
})
```
### Python :
```python
x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificateForPublic", {
hash: string
})
```
### PHP :
```php
$x = Hxt("HxtidzPdZKVujcQv1Yy7jYgwnQ5YgiBmWSmgCYXq7cKU4DLf2Ak", "GetCertificateForPublic", array(
"hash" => string
));
```