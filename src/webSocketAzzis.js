((g) => {
  var Connect = () => {
    if (Connect.socket === undefined) Connect.socket = null;
    return new Promise((resolve, reject) => {
      if (Connect.socket === null) {
        var socket = new WebSocket("{{.host}}");
        Connect.socket = socket;

        socket.onopen = () => {
          resolve(socket);
        };

        socket.onerror = (e) => {
          reject(e);
        };
      } else {
        resolve(Connect.socket);
      }
    });
  };

  var Call = (xContract, function_name, args) => {
    if (args === undefined) args = {};

    return new Promise((resolve, reject) => {
      Connect().then((sock) => {
        if (sock == null) {
          Call(xContract, function_name, args).then((e) => {
            resolve(e);
          });
        } else {
          sock.onmessage = (e) => {
            resolve(JSON.parse(e.data));
          };
          var options = {
            xContract: xContract,
            xContract_function: function_name,
            xContract_args: args,
          };

          try {
            sock.send(JSON.stringify(options));
          } catch (Error) {
            Connect.socket = null;
            Call(xContract, function_name, args).then((e) => {
              resolve(e);
            });
          }
        }
      });
    });
  };

  var CallStd = (function_name, args) => {
    if (args === undefined) args = {};

    return new Promise((resolve, reject) => {
      Connect().then((sock) => {
        if (sock == null) {
          CallStd(function_name, args).then((e) => {
            resolve(e);
          });
        } else {
          sock.onmessage = (e) => {
            resolve(JSON.parse(e.data));
          };
          var options = {
            xContract: "",
            xContract_function: function_name,
            xContract_args: args,
          };

          try {
            sock.send(JSON.stringify(options));
          } catch (Error) {
            Connect.socket = null;
            CallStd(function_name, args).then((e) => {
              resolve(e);
            });
          }
        }
      });
    });
  };

  g.HxtStd = CallStd;
  g.HxtStd.c = Connect;

  g.Hxt = Call;
  g.Hxt.c = Connect;
})(typeof global == "undefined" ? window : global);
