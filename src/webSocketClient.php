<?php

  include "./vendor/autoload.php";
  include "consolelog.php";

  function Hxt($xContract, $functionName, $args = "{}")
  {
    $client = new WebSocket\Client("wss://ws.hexantium.com/ws", [
      'timeout' => 10,
    ]);

    if ($args) {
      $objectArgs = (object) $args;
      $encodeArgs = json_encode($objectArgs);
      // print_r($encodeArgs);
      // print_r(gettype($encodeArgs));
    }

    $client->text(
      sprintf('{"xContract" : "%s", "xContract_function" : "%s", "xContract_args" : %s}', 
      $xContract, 
      $functionName, 
      $args === "{}" ? $args : $encodeArgs
    ));
    
    try {
      $message = json_decode($client->receive());
      // print_r($message);
      // console_log($message);
      return $message;
      
    } catch (\WebSocket\ConnectionException $e) {
      // Possibly log errors
      print_r("Error: ".$e->getMessage());
    } catch (\WebSocket\TimeoutException $e) {
      // Possibly log errors
      print_r("Error: ".$e->getMessage());
    }
      
    $client->close();
  }
    
  function HxtStd($functionName, $args = "{}")
  {
    $client = new WebSocket\Client("wss://ws.hexantium.com/ws", [
      'timeout' => 10,
    ]);

    if ($args) {
      $objectArgs = (object) $args;
      $encodeArgs = json_encode($objectArgs);
      // print_r($encodeArgs);
      // print_r(gettype($encodeArgs));
    }
    
    $client->text(
      sprintf('{"xContract" : "", "xContract_function" : "%s", "xContract_args" : %s}', 
      $functionName, 
      $args === "{}" ? $args : $encodeArgs
    ));
    
    try {
      $message = json_decode($client->receive());
      // print_r($message);
      // console_log($message);
      return $message;
      
    } catch (\WebSocket\ConnectionException $e) {
      // Possibly log errors
      print_r("Error: ".$e->getMessage());
    } catch (\WebSocket\TimeoutException $e) {
      // Possibly log errors
      print_r("Error: ".$e->getMessage());
    }

    $client->close();
  }
  
?>